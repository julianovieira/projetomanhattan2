/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public interface Acao {

    /* request pega o conteúdo do site, como request.getParameterByName() */ 
    
    String executar(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
