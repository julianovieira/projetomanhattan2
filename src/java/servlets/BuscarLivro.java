/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Controller.Acao;
import dao.DAOLivro;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public class BuscarLivro implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOLivro daoLivros = new DAOLivro();        
        String nome = request.getParameter("nome");
        request.setAttribute("autores", daoLivros.getLivroPeloTitulo(nome));
        return "/listarLivros.jsp";
    }

}
