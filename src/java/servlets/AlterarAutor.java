/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Controller.Acao;
import bean.Autor;
import dao.DAOAutor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public class AlterarAutor implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOAutor daoAutor = new DAOAutor();        
        Autor autor = new Autor();
        autor.setNome(request.getParameter("nome"));
        String codigo = request.getParameter("codigo");
        if(codigo == null || codigo.isEmpty())
        {
            daoAutor.addAutor(autor.getNome());
        }
        else
        {
            autor.setCodigo(Integer.parseInt(codigo));            
            daoAutor.alterarAutor(autor);
        }
        request.setAttribute("autores", DAOAutor.getAutores());
        return "/listarAutores.jsp";
    }

}
