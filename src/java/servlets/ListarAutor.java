/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Controller.Acao;
import dao.DAOAutor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public class ListarAutor implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOAutor daoAutor = new DAOAutor();        
        //String nome = request.getParameter("nome");
        request.setAttribute("autores", daoAutor.getAutores());
        //request.setAttribute("autores", daoAutor.getAutoresPeloNome(nome));
        return "/listarAutores.jsp";
    }

}
