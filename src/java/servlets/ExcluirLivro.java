/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Controller.Acao;
import dao.DAOAutor;
import dao.DAOLivro;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public class ExcluirLivro implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOLivro daoLivro = new DAOLivro();        
        String codigo = request.getParameter("codigo");
        daoLivro.deletarLivro(codigo);
        request.setAttribute("livros", daoLivro.getLivros());
        return "/listarLivros.jsp";
    }

}
