<%-- 
    Document   : livro
    Created on : 21/02/2017, 20:35:13
    Author     : juliano.vieira
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css"
            href="css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
            
        <title>Adicionar livro</title>
    </head>
<body>
    <p><a href="index.jsp">Voltar</a></p>
    
    <form method="POST" action='Controller?action=AlterarLivro' name="formAdicionarLivro">
        <table>
            <tr>
                <td>
                    ISBN : 
                </td>
                <td>
                    <input type="text" readonly="readonly" name="isbn"
                        value="<c:out value="${livro.isbn}" />" /> 
                </td>
            </tr>
            <tr>
                <td>
                    Título : 
                </td>
                <td>
                    <input type="text" name="nome"
                        value="<c:out value="${livro.titulo}" />" />
                </td>
            </tr>
            <tr>
                <td>
                    Descrição : 
                </td>
                <td>
                    <input type="text" name="descricao"
                        value="<c:out value="${livro.descricao}" />" />
                </td>
            </tr>
            <tr>
                <td>
                    Autores: 
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
                    Editora : 
                </td>
                <td>
                    <input type="text" name="editora"
                        value="<c:out value="${livro.editora}" />" />
                </td>
            </tr>
        </table>
        <input type="submit" value="Salvar" />
    </form>
</body>
</html>